/**
 * 
 */
package smartdias.vitamins;

/**
 * @author murli_000
 * 
 */
public class ApplicationLookupManager {

	private static ApplicationLookupManager applicationLookupManager = null;

	/**
	 * 
	 */
	private ApplicationLookupManager() {

	}

	public static ApplicationLookupManager getInstance() {
		if (applicationLookupManager == null) {
			applicationLookupManager = new ApplicationLookupManager();
		}
		return applicationLookupManager;
	}
}
